---
version: 2
titre: Alambic temporel
abréviation: Alambic-temporel
filières:
  - Informatique
nombre d'étudiants: 1
mots-clé: [Logique temporelle, Javascript, Angular, Bootstrap]
langue: [E,F]
type de projet: Projet de semestre 5
année scolaire: 2020/2021
confidentialité: non
suite: non
---
## Contexte

La logique temporelle permet d'exprimer des propriétés sur des "signaux booléens" (évolution au fil du temps de
variables booléennes).

En lien avec le cours de Maths Spécifiques, l'[Alambic Temporel](http://frederic.bapst.home.hefr.ch/formal/alambic/) est une application Web développée par un étudiant en 2016, permettant de manipuler des formules temporelles. Elle est basée sur les technologies Angular, Bootstrap, d3 et Bower. Si cette ressource intuitive s'intègre parfaitement aux exercices du cours et semble bien appréciée des étudiants, nous lui trouvons quand même des défauts/limitations et envisageons diverses améliorations.

## Objectifs

Le but de ce projet est de mettre à jour cette application, en effectuant quelques tâches de maintenance et aussi en proposant de nouvelles fonctionnalités. Il s'agira typiquement de considérer des aspects tels que : 

- Vérifier la compatibilité avec les dernières versions des librairies

- Passer sur un autre gestionnaire de dépendances (Bower ne semble plus recommandé), peut-être porter le projet sur un autre IDE (p. ex. WebStorm au lieu d'Eclipse)

- Reconsidérer la distinction entre "formule" et "signal" — il serait naturel de considérer qu'un signal est une formule (la plus élémentaire possible)

- Envisager de déposer le code sur github, voire y déployer l'application

- Isoler le noyau "métier" dans une librairie de logique temporelle, et étendre les fonctionnalités comme suggéré dans les points suivants

- Offrir une simplification automatique de la description des signaux périodiques

- Permettre une forme de "minimisation" de signaux qui préserve l'enchaînement des transitions (le signal "010…" présente la même allure que le signal "0001100…")

- Synthétiser une formule pour décrire un signal donné

- Déterminer si une formule est satisfaisable, resp. si c'est une tautologie, en synthétisant au passage un éventuel contre-exemple

- Ajouter de nouveaux opérateurs (comme le "next"), et réviser la syntaxe de description des formules

La nouvelle réalisation devra être au moins aussi utilisable que l'ancienne ;) 

<!---

`inline code`
[GitHub](http://github.com)

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

*emphasized*, **bold**
Just trying to put a comment...

$ git add *.md; git commit -m "wip"; git push

Meta données				
titre	Texte	obligatoire	1	Max 255 caractères
Abréviation	Texte	optionnel	1	Max 255 caractères
filières	Choix	obligatoire	1..2	[Informatique ; Télécommunications]
orientations	Choix	optionnel	1..3	[Internet et communication ; Réseaux et sécurité]
langue	Lite de Choix	obligatoire	1	[D;F;E]
professeurs co-superviseurs	Texte	obligatoire	1..2	texte ouvert (Max 255 carcatères)
assistants	Texte	optionnel	0..n	Texte ouvert (Max 255 carcatères)
proposé par étudiant	Texte	optionnel	1	Texte ouvert (Max 255 carcatères)
mandants	Texte	optionnel	1..n	Texte iuvert  + URL
instituts	Choix	optionnel	1..n	[HumanTech; iCoSys; iSIS; ENERGY; ChemTech ;iPrint, iRAP; iTEC, SeSi; TRANSFORM]
confidentialité	Binaire	obligatoire	Oui/Non	[Oui; Non]
réalisation	Choix	optionnel	1..n	[labo;entreprise; étranger; instituts externe]
mots-clé	Liste	optionnel	1..n	Termes séparés par des virgule
nombre d'étudiants	Nombre	obligatoire	1..2	[1; 2]
suite	Binaire	obligatoire	Oui/Non	[Oui; Non]
Markdown				
Contexte	Texte	obligatoire	1	
Objectifs	Texte	obligatoire	1	
Contraintes	Teste	optionel	1	
Extrait du répertoire				
Type de projet	Choix	obligatoire	1	Tiré du répertoire [semestre ; bachelor]
Professeur principal	Choix	Obligatoire		Tiré du répertoire

 ```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/myimage.jpg}
\end{center}
```

-->


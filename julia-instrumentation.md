---
version: 2
titre: Instrumentation de code Julia
abréviation: Julia-instrumentation
filières:
  - Informatique
nombre d'étudiants: 1
mots-clé: [Instrumentation, Julia, Scientific computing]
langue: [E,F]
type de projet: Projet de semestre 5
année scolaire: 2020/2021
confidentialité: non
suite: non
---
## Contexte

Dans le domaine du calcul scientifique, le langage de programmation Julia est un candidat bien placé en tant qu'alternative à Python, Matlab ou C++. Nous aimerions déterminer dans quelle mesure Julia pourrait être équipé avec un équivalent de [COJAC](https://github.com/Cojac/Cojac/wiki) pour Java, offrant la possibilité de modifier le comportement numérique des programmes au moment de l'exécution. 

Certaines librairies Julia comme [Cassette](https://github.com/jrevels/Cassette.jl) semblent offrir une infrastructure pour modifier du code Julia (au niveau de l'arbre syntaxique abstrait ou peut-être de la représentation intermédiaire du compilateur LLVM).

## Objectifs

Il s'agit de proposer une base de code qui permette d'instrumenter du code Julia pour modifier le comportement numérique des programmes. L'approche sera illustrée en réalisant un prototype rudimentaire destiné à détecter quelques anomalies numériques (overflow, underflow, absorption, annulation), et en l'appliquant sur des programmes de démonstration.

Nous commencerons par une phase d'analyse axée sur deux points : 

- Identifier ce qui distingue l'environnement Julia de celui de Java pour ce qui est des aspects numériques, et estimer quelles fonctionnalités de COJAC gardent toute leur pertinence dans le langage/écosystème Julia.

- Trouver des mécanismes susceptibles d'offrir quelque chose d'analogue à la technologie Java-agent utilisée par COJAC — des outils d'instrumentation de code Julia, ou d'autres qui agiraient au niveau du compilateur LLVM sur lequel Julia est bâti.



<!---

`inline code`
[GitHub](http://github.com)

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

*emphasized*, **bold**
Just trying to put a comment...

$ git add *.md; git commit -m "wip"; git push

Meta données				
titre	Texte	obligatoire	1	Max 255 caractères
Abréviation	Texte	optionnel	1	Max 255 caractères
filières	Choix	obligatoire	1..2	[Informatique ; Télécommunications]
orientations	Choix	optionnel	1..3	[Internet et communication ; Réseaux et sécurité]
langue	Lite de Choix	obligatoire	1	[D;F;E]
professeurs co-superviseurs	Texte	obligatoire	1..2	texte ouvert (Max 255 carcatères)
assistants	Texte	optionnel	0..n	Texte ouvert (Max 255 carcatères)
proposé par étudiant	Texte	optionnel	1	Texte ouvert (Max 255 carcatères)
mandants	Texte	optionnel	1..n	Texte iuvert  + URL
instituts	Choix	optionnel	1..n	[HumanTech; iCoSys; iSIS; ENERGY; ChemTech ;iPrint, iRAP; iTEC, SeSi; TRANSFORM]
confidentialité	Binaire	obligatoire	Oui/Non	[Oui; Non]
réalisation	Choix	optionnel	1..n	[labo;entreprise; étranger; instituts externe]
mots-clé	Liste	optionnel	1..n	Termes séparés par des virgule
nombre d'étudiants	Nombre	obligatoire	1..2	[1; 2]
suite	Binaire	obligatoire	Oui/Non	[Oui; Non]
Markdown				
Contexte	Texte	obligatoire	1	
Objectifs	Texte	obligatoire	1	
Contraintes	Teste	optionel	1	
Extrait du répertoire				
Type de projet	Choix	obligatoire	1	Tiré du répertoire [semestre ; bachelor]
Professeur principal	Choix	Obligatoire		Tiré du répertoire

 ```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/myimage.jpg}
\end{center}
```

-->


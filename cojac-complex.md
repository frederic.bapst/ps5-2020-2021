---
version: 2
titre: Cojac - Des complexes à assumer
abréviation: Cojac-complex
filières:
  - Informatique
nombre d'étudiants: 1
mots-clé: [Cojac, Nombres, Instrumentation, Java, Scientific computing]
langue: [E,F]
type de projet: Projet de semestre 5
année scolaire: 2020/2021
confidentialité: non
suite: non
---
## Contexte

[COJAC](https://youtu.be/eAy71M34U_I?list=PLHLKWUtT0B7kNos1e48vKhFlGAXR1AAkF) est un utilitaire développé à la HEIA-FR qui instrumente du bytecode Java pour enrichir à l'exécution le comportement numérique d'un programme (détection des overflows et autres anomalies, calcul par intervalles, précision arbitrairement grande, etc).

Une extension naturelle du modèle arithmétique de Java serait de considérer les nombres complexes. L'idée (originale à notre connaissance) serait de programmer avec le type `double` usuel, puis de pouvoir exécuter le programme dans un mode particulier où les calculs adoptent le comportement des nombres complexes. 


## Objectifs

Il s'agit d'offrir via COJAC la fonctionnalité d'un remplacement automatique, au runtime, des nombres à virgule flottante par des nombres complexes dans un programme Java. Deux pistes distinctes pourront être explorées, par l'ajout respectif d'une nouvelle sorte de : 

- [`Wrapper`](https://github.com/Cojac/Cojac/wiki#4---cojac-the-enriching-wrapper), c'est-à-dire que les double sont remplacés par des objets;

- comportement des `double`, où on conserve le type primitif `double` mais pour y "cacher" la partie imaginaire dans les bits les moins significatifs de la mantisse — une astuce déjà illustrée avec un modèle de calcul par intervalles.

Dans les deux cas, les opérations sur les nombres à virgule seront redéfinies pour prendre en compte la partie imaginaire du nombre complexe. L'implémentation sera complétée par des programmes de démonstration illustrant le potentiel de l'approche, et des mesures de performance.


<!---

https://github.com/Cojac/Cojac/wiki
https://youtu.be/eAy71M34U_I?list=PLHLKWUtT0B7kNos1e48vKhFlGAXR1AAkF

`inline code`
[GitHub](http://github.com)

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

*emphasized*, **bold**
Just trying to put a comment...
l'[Alambic Temporel](http://.../alambic/)

$ git add *.md; git commit -m "wip"; git push

Meta données				
titre	Texte	obligatoire	1	Max 255 caractères
Abréviation	Texte	optionnel	1	Max 255 caractères
filières	Choix	obligatoire	1..2	[Informatique ; Télécommunications]
orientations	Choix	optionnel	1..3	[Internet et communication ; Réseaux et sécurité]
langue	Lite de Choix	obligatoire	1	[D;F;E]
professeurs co-superviseurs	Texte	obligatoire	1..2	texte ouvert (Max 255 carcatères)
assistants	Texte	optionnel	0..n	Texte ouvert (Max 255 carcatères)
proposé par étudiant	Texte	optionnel	1	Texte ouvert (Max 255 carcatères)
mandants	Texte	optionnel	1..n	Texte iuvert  + URL
instituts	Choix	optionnel	1..n	[HumanTech; iCoSys; iSIS; ENERGY; ChemTech ;iPrint, iRAP; iTEC, SeSi; TRANSFORM]
confidentialité	Binaire	obligatoire	Oui/Non	[Oui; Non]
réalisation	Choix	optionnel	1..n	[labo;entreprise; étranger; instituts externe]
mots-clé	Liste	optionnel	1..n	Termes séparés par des virgule
nombre d'étudiants	Nombre	obligatoire	1..2	[1; 2]
suite	Binaire	obligatoire	Oui/Non	[Oui; Non]
Markdown				
Contexte	Texte	obligatoire	1	
Objectifs	Texte	obligatoire	1	
Contraintes	Teste	optionel	1	
Extrait du répertoire				
Type de projet	Choix	obligatoire	1	Tiré du répertoire [semestre ; bachelor]
Professeur principal	Choix	Obligatoire		Tiré du répertoire

 ```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/myimage.jpg}
\end{center}
```

-->

